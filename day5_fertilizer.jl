function save_map(map_number, input_lines, empty_lines)
    map_data = []
    start_row = empty_lines[map_number]+2
    if map_number == 7
        stop_row = length(input_lines)
    else
        stop_row = empty_lines[map_number+1]-1
    end
    for i in range(start_row, stop_row)
        push!(map_data, [parse(Int, s) for s in split(input_lines[i])])
    end
    return map_data
end

function get_next_index_from_map(current_index, map_data)
    next_index = current_index
    for m in map_data
        if current_index >= m[2] && current_index < m[2] + m[3]
            offset = current_index - m[2]
            next_index = m[1] + offset
        end
    end
    return next_index
end

# create empty arrays to store empty row numbers
empty = []
# open input
lines = readlines("day5_input.txt")
# read each line, finding empty rows
i = 0
while i < length(lines)
    global i += 1
    if lines[i] == ""
        push!(empty, i)
    end
end

# parse data
seeds = [parse(Int, s) for s in split(lines[1])[2:end]]
seeds_to_soil_map_data = save_map(1, lines, empty)
soil_to_fertilizer_map_data = save_map(2, lines, empty)
fertilizer_to_water_map_data = save_map(3, lines, empty)
water_to_light_map_data = save_map(4, lines, empty)
light_to_temperature_map_data = save_map(5, lines, empty)
temperature_to_humidity_map_data = save_map(6, lines, empty)
humidity_to_location_map_data = save_map(7, lines, empty)

# step through maps to get location
location = []
for s in seeds
    soil = get_next_index_from_map(s, seeds_to_soil_map_data)
    fertilizer = get_next_index_from_map(soil, soil_to_fertilizer_map_data)
    water = get_next_index_from_map(fertilizer, fertilizer_to_water_map_data)
    light = get_next_index_from_map(water, water_to_light_map_data)
    temperature = get_next_index_from_map(light, light_to_temperature_map_data)
    humidity = get_next_index_from_map(temperature, temperature_to_humidity_map_data)
    l = get_next_index_from_map(humidity, humidity_to_location_map_data)
    push!(location, l)
end

# report closest location
println(minimum(location))