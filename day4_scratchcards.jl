# constants
winning_numbers = 10
# empty array to store card values
values = []
# open input
lines = readlines("day4_input.txt")
# array to track number cards
card_count = ones(size(lines))
# read each line
i = 0
while i < length(lines)
    global i += 1
    # find numbers
    pattern = r"(\d+)"
    numbers = []
    for m in eachmatch(pattern, lines[i])
        push!(numbers, parse(Int, m.match))
    end
    # iterate over range of winning numbers checking if any of the remaining numbers match
    winning = numbers[2:2+winning_numbers-1]
    playing = numbers[2+winning_numbers:end]
    value = 0
    matches = 0
    for w in winning
        for p in playing
            if w == p
                if value == 0
                    value = 1
                else
                    value *= 2
                end
                matches += 1
            end
        end
    end
    push!(values, value)
    # for every N matchex, add one copy to each of the N following cards
    to_copy = i+1:1:i+matches
    j = 1
    while j <= card_count[i]
        for c in to_copy
            card_count[c] += 1
        end
        j += 1
    end
end

println(sum(values))
println(sum(card_count))