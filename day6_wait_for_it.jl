function calculate_wins(race_time, max_distance)
    # coefficients of quadratic equation
    a = -1
    b = race_time
    c = -max_distance
    # solve
    solutions = [(-b - sqrt(b^2 - 4*a*c))/(2*a), (-b + sqrt(b^2 - 4*a*c))/(2*a)]
    # convert to integers and adjust solutions if they result in a tie
    lower = Int(ceil(minimum(solutions)))
    upper = Int(floor(maximum(solutions)))
    if lower*(race_time - lower) == max_distance
        lower += 1
    end
    if upper*(race_time - upper) == max_distance
        upper -= 1
    end 
    # caluclate wins
    return upper - lower + 1
end

# open input
lines = readlines("day6_input.txt")
# parse data
times = [parse(Int, s) for s in split(lines[1])[2:end]]
distances = [parse(Int, s) for s in split(lines[2])[2:end]]

# loop over races, recording number of wins
total_wins = []
for i in range(1,length(times))
    push!(total_wins, calculate_wins(times[i], distances[i]))
end

# multiply wins together
mult = 1
for w in total_wins
    global mult *= w
end
println(mult)

# concatenate times and distances for part 2
for i in range(1,length(times))
    if i == 1
        time = string(times[1])
        distance = string(distances[1])
    else
        time = string(time,times[i])
        distance = string(distance,distances[i])
    end
    if i == length(times)
        time = parse(Int, time)
        distance = parse(Int, distance)
        println(calculate_wins(time, distance))
    end
end