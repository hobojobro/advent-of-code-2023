function count_steps(start, destination, LR, from, L, R)
    i_LR = 0
    steps = 0
    while start != destination
        # get next turn
        i_LR += 1
        if i_LR > length(LR)
            i_LR = 1
        end
        next_turn = LR[i_LR]
        # get current location
        i_from = findall(f==start for f in from)
        # get next location
        if next_turn == 'L'
            start = L[i_from][1]
        else
            start = R[i_from][1]
        end
        # count step
        steps += 1
        # 
        if destination == 'Z' && start[3] == 'Z'
            destination = start
        end
    end
    return steps
end

function gcd(a, b)
    while b != 0
        temp = b
        b = a % b
        a = temp
    end
    return a
end

function lcm(a, b)
    return a/gcd(a,b)*b
end

function lcmm(array)
    ans = lcm(array[1], array[2])
    for i in range(3,length(array))
        ans = lcm(ans, array[i])
    end
    return ans
end

# open input
lines = readlines("day8_input.txt")
# parse
LR = lines[1]
from = String[]
L = String[]
R = String[]
for l in lines[3:end]
    s = split(l)
    push!(from, s[1])
    push!(L, s[3][2:4])
    push!(R, s[4][1:3])
end

# step thru map from AAA to ZZZ
start = "AAA"
destination = "ZZZ"
steps = count_steps(start, destination, LR, from, L, R)
println(steps)

# now find all nodes that end in A
starts = from[findall(f[3]=='A' for f in from)]
destination = 'Z'
# step through each path until they reach nodes ending in Z
steps = []
for i in range(1,length(starts))
    push!(steps, count_steps(starts[i], destination, LR, from, L, R))
end
println(lcmm(steps))