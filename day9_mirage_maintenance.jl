function diff_sequence(sequence)
    diff = []
    for i in range(2,length(sequence))
        push!(diff, sequence[i] - sequence[i-1])
    end
    return diff
end

function extrapolate_sequence(sequence)
    sequence = [sequence]
    zeros = []
    while length(zeros) != length(sequence[end])
        push!(sequence, diff_sequence(sequence[end]))
        zeros = findall(n==0 for n in sequence[end])
    end
    for i in range(1, length(sequence)-1)
        j = length(sequence)-i
        push!(sequence[j], sequence[j][end]+sequence[j+1][end])
    end
    return sequence[1][end]
end

# open input
lines = readlines("day9_input.txt")
extrapolated = []
previous = []
# parse and extrapolate, both forwards and backwards
for i in range(1, length(lines))
    sequence = [parse(Int, s) for s in split(lines[i])]
    push!(extrapolated, extrapolate_sequence(sequence))
    sequence = reverse(sequence)
    push!(previous, extrapolate_sequence(sequence))
end

println(sum(extrapolated))
println(sum(previous))