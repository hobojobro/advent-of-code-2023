#empty arrays to store part numbers and gear ratios
parts = []
gear_ratios = []
# load entire file
lines = readlines("day3_input.txt")
i = 0
# read each line
while i < length(lines)
    # set current and adjacent lines
    global i += 1 #this is stupid?
    line_current = lines[i]
    if i == 1
        line_prev = ""
        line_next = lines[i+1]
    elseif i == size(lines, 1)
        line_prev = lines[i-1]
        line_next = ""
    else
        line_prev = lines[i-1]
        line_next = lines[i+1]
    end
    # get numbers on all three lines
    pattern = r"(\d+)"
    numbers_prev = []
    numbers_prev_offset = []
    for m in eachmatch(pattern, line_prev)
        push!(numbers_prev, m.match) #parse(Int, m.match)
        push!(numbers_prev_offset, m.offset)
    end
    numbers_current = []
    numbers_current_offset = []
    for m in eachmatch(pattern, line_current)
        push!(numbers_current, m.match) #parse(Int, m.match)
        push!(numbers_current_offset, m.offset)
    end
    numbers_next = []
    numbers_next_offset = []
    for m in eachmatch(pattern, line_next)
        push!(numbers_next, m.match) #parse(Int, m.match)
        push!(numbers_next_offset, m.offset)
    end
    # get symbols on all three lines
    pattern = r"[^\d\.]"
    symbols_prev_offset = []
    for m in eachmatch(pattern, line_prev)
        push!(symbols_prev_offset, m.offset)
    end
    symbols_current_offset = []
    for m in eachmatch(pattern, line_current)
        push!(symbols_current_offset, m.offset)
    end
    symbols_next_offset = []
    for m in eachmatch(pattern, line_next)
        push!(symbols_next_offset, m.offset)
    end
    # get just * on current line
    pattern = r"\*"
    stars_offset = []
    for m in eachmatch(pattern, line_current)
        push!(stars_offset, m.offset)
    end
    # save numbers with adjacent symbols
    j = 1
    while j <= length(numbers_current)
        save_number = 0
        min = numbers_current_offset[j] - 1
        max = numbers_current_offset[j] + length(numbers_current[j])
        # previous
        for o in symbols_prev_offset
            if o >= min && o <= max
                save_number = 1
            end
        end
        # current
        for o in symbols_current_offset
            if o == min || o == max
                save_number = 1
            end
        end
        # next
        for o in symbols_next_offset
            if o >= min && o <= max
                save_number = 1
            end
        end
        # save
        if save_number == 1
            push!(parts, parse(Int, numbers_current[j]))
        end
        j += 1
    end
    # save gear ratios
    j = 1
    while j <= length(stars_offset)
        save_number = 0
        gears = []
        # previous
        k = 1
        while k <= length(numbers_prev)
            min = numbers_prev_offset[k] - 1
            max = numbers_prev_offset[k] + length(numbers_prev[k])
            if stars_offset[j] >= min && stars_offset[j] <= max
                push!(gears, parse(Int, numbers_prev[k]))
            end
            k += 1
        end
        # current
        k = 1
        while k <= length(numbers_current)
            min = numbers_current_offset[k] - 1
            max = numbers_current_offset[k] + length(numbers_current[k])
            if stars_offset[j] >= min && stars_offset[j] <= max
                push!(gears, parse(Int, numbers_current[k]))
            end
            k += 1
        end
        # next
        k = 1
        while k <= length(numbers_next)
            min = numbers_next_offset[k] - 1
            max = numbers_next_offset[k] + length(numbers_next[k])
            if stars_offset[j] >= min && stars_offset[j] <= max
                push!(gears, parse(Int, numbers_next[k]))
            end
            k += 1
        end
        # save gear ratio of exactly 2 gears
        if length(gears) == 2
            push!(gear_ratios, gears[1]*gears[2])
        end
        j += 1
    end
end

# print sum of part numbers and sum of gear ratios
println(sum(parts))
println(sum(gear_ratios))