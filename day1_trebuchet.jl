#create empty array to store sum for each line
sums = []
# open input
file = open("day1_input.txt")
# read each line
for l in eachline(file)
    # find numeric values (digits or spelled out)
    pattern = r"[0-9]|(one|two|three|four|five|six|seven|eight|nine)"
    digits = []
    for m in eachmatch(pattern, l)
        # this is awful...
        if m.match == "one"
            push!(digits, 1)
        elseif m.match == "two"
            push!(digits, 2)
        elseif m.match == "three"
            push!(digits, 3)
        elseif m.match == "four"
            push!(digits, 4)
        elseif m.match == "five"
            push!(digits, 5)
        elseif m.match == "six"
            push!(digits, 6)
        elseif m.match == "seven"
            push!(digits, 7)
        elseif m.match == "eight"
            push!(digits, 8)
        elseif m.match == "nine"
            push!(digits, 9)
        else
            push!(digits, parse(Int, m.match) )
        end
    end
    # concatenate first and last digits
    s = digits[1]*10 + digits[end]
    push!(sums, s)
end

#print sum of sums
println(sum(sums))