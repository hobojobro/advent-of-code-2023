# constants
max_red = 12
max_green = 13
max_blue = 14
# empty arrays to store game numbers and powers
games = []
powers = []
# open input
file = open("day2_input.txt")
# read each line
for l in eachline(file)
    # find numbers
    pattern = r"(\d+)"
    numbers = []
    for m in eachmatch(pattern, l)
        push!(numbers, parse(Int, m.match))
    end
    # find colors
    pattern = r"(red|green|blue)"
    colors = []
    for m in eachmatch(pattern, l)
        push!(colors, m.match)
    end
    # numbers should always have one more element than colors (the game number)
    if size(numbers, 1) != size(colors, 1) + 1
        throw(ErrorException("numbers array should have one more element than colors"))
    end
    # check that number for each color is below the limit
    i = 1
    this_red = 0
    this_green = 0
    this_blue = 0
    while i <= size(colors, 1)
        if colors[i] == "red" && numbers[i+1] > this_red
            this_red = numbers[i+1]
        elseif colors[i] == "green" && numbers[i+1] > this_green
            this_green = numbers[i+1]
        elseif colors[i] == "blue" && numbers[i+1] > this_blue
            this_blue = numbers[i+1]
        end
        i += 1
    end
    # save game number if none of the numbers were too high
    if this_red <= max_red && this_green <= max_green && this_blue <= max_blue
        push!(games, numbers[1])
    end
    push!(powers, this_red*this_green*this_blue)
end

#print sum of saved game numbers
println(sum(games))
println(sum(powers))