function categorize_hand(hand)
    cards = split(hand, "")
    match = []
    i = 0
    joker = 0
    while i < length(cards)
        i += 1
        j = i + 1
        # check for jokers
        if cards[i] == "J"
            joker += 1
        end
        while j <= length(cards)
            if cards[i] == cards[j]
                if length(match) == 0
                    push!(match, [cards[i], 2])
                elseif length(match) == 1
                    if cards[i] == match[1][1]
                        match[1][2] += 1
                    else
                        push!(match, [cards[i], 2])
                    end
                else
                    if cards[i] == match[1][1]
                        match[1][2] += 1
                    elseif cards[i] == match[2][1]
                        match[2][2] += 1
                    else
                        throw(Exception)
                    end
                end
                #remove j from array of cards since it's already been matched
                if cards[j] == "J"
                    joker += 1
                end
                deleteat!(cards, j)
            else
                j += 1
            end
        end
    end
    # set joker to maximize hand
    if joker > 0
        if length(match) == 0
            # high card w/ 1 joker, make a pair
            push!(match, ["J", 2])
        elseif length(match) == 1 && match[1][2] < 5
            # add all jokers to existing match, unless the match is jokers, then just add 1
            if match[1][1] == "J"
                match[1][2] += 1
            else
                match[1][2] += joker
            end
        elseif length(match) == 2
            # add single joker to either pair to make a full house
            if joker == 1
                match[1][2] += 1
            # jokers are a match; combine with other match and remove joker row
            else
                if match[1][1] == "J"
                    match[2][2] += match[1][2]
                    deleteat!(match, 1)
                else
                    match[1][2] += match[2][2]
                    deleteat!(match, 2)
                end
            end
        end
    end
    # categorize
    if length(match) == 0
        type = "high-card"
    elseif match[1][2] == 5
        type = "five-of-a-kind"
    elseif match[1][2] == 4
        type = "four-of-a-kind"
    elseif match[1][2] == 3
        if length(match) == 1
            type = "three-of-a-kind"
        else
            type = "full-house"
        end
    elseif match[1][2] == 2
        if length(match) == 1
            type = "one-pair"
        elseif match[2][2] == 3
            type = "full-house"
        else
            type = "two-pair"
        end
    end
    return type
end

function compare_hands(hands, types)
    # sorted arrays
    all_types = ["five-of-a-kind", "four-of-a-kind", "full-house", "three-of-a-kind", "two-pair", "one-pair", "high-card"]
    all_cards = ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"]
    # use types if possible
    if types[1] != types[2]
        for t in all_types
            if t == types[1]
                higher = 1
                break
            elseif t == types[2]
                higher = 2
                break
            end
        end
    # otherwise check card values from L to R
    else
        cards = [split(hands[1], ""), split(hands[2], "")]
        higher = 0
        for i in range(1,length(cards[1]))
            if cards[1][i] != cards[2][i] && higher == 0
                for c in all_cards
                    if c == cards[1][i]
                        higher = 1
                        break
                    elseif c == cards[2][i]
                        higher = 2
                        break
                    end
                end
            end
        end
    end
    return higher
end

hand = []
bid = []
type = []
# open input
lines = readlines("day7_input.txt")

for l in lines
    # parse data
    split_line = split(l)
    push!(hand, split_line[1])
    push!(bid, parse(Int, split_line[2]))
    # get type of hand
    push!(type, categorize_hand(split_line[1]))
end

#functions to:
#1 compare hands
#3 find higher value card, starting from the beginning

# compare all hands, creating new arrays sorted weakest to strongest, so index is equal to rank
hand_sorted = []
bid_sorted = []
type_sorted = []
for i in range(1,length(hand))
    if i == 1
        push!(hand_sorted, hand[i])
        push!(bid_sorted, bid[i])
        push!(type_sorted, type[i])
    else
        j = 1
        while compare_hands([hand[i], hand_sorted[j]], [type[i], type_sorted[j]]) == 1 && j < length(hand_sorted)
            j += 1
        end
        if j == length(hand_sorted) && compare_hands([hand[i], hand_sorted[j]], [type[i], type_sorted[j]]) == 1
            push!(hand_sorted, hand[i])
            push!(bid_sorted, bid[i])
            push!(type_sorted, type[i])
        else
            insert!(hand_sorted, j, hand[i])
            insert!(bid_sorted, j, bid[i])
            insert!(type_sorted, j, type[i])
        end
    end
end
 
# calculate winnings
winnings = 0
for i in range(1,length(bid_sorted))
    global winnings += i*bid_sorted[i]
end

println(winnings)